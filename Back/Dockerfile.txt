FROM node:10

WORKDIR "/opt/app"

COPY . ./

EXPOSE 3000

RUN npm install
RUN npm run build

# connection au serveur amqp
ENV AMQP_HOST = localhost
ENV AMQP_PORT = 5672
ENV AMQP_USER = guest
ENV AMQP_PASSWORD = guest
# connection à la base de données
ENV MONGO_HOST = mongo
ENV MONGO_PORT = 27017

CMD npm start

